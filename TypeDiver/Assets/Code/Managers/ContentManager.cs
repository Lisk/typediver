﻿using Assets.Code.DataStructures;
using Assets.Code.DataStructures.Abstract;
using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using UnityEngine;

namespace Assets.Code.Managers
{
    public static class ContentManager
    {
        static Dictionary<Type, Dictionary<string, object>> contentCache = new Dictionary<Type, Dictionary<string, object>>();

        static ContentManager()
        {
            using (StringReader sr = new StringReader(((TextAsset)Resources.Load("Data/fish")).text))
            {
                ParseCsv<FishProperties>(sr);
            }
        }

        #region Core
        static void ParseCsv<T>(StringReader sr) where T : CsvProperties
        {
            contentCache[typeof(T)] = new Dictionary<string, object>();

            using (var csv = new CsvReader(sr, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<T>();
                foreach(var record in records)
                {
                    contentCache[typeof(T)][record.Id] = record;
                }
            }
        }

        /// <summary>
        /// Core function. This is private since we prefer to have explicit shortcuts to make life easy for the caller
        /// rather than relying on the caller to know what to cast to. i.e. https://i.imgur.com/2VLzs3Z.png
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="id"></param>
        /// <returns></returns>
        static object GetProperties<T>(string id)
        {
            if (!contentCache[typeof(T)].ContainsKey(id))
                throw new Exception("Missing properties for id: " + id + ", T: " + typeof(T));
            return contentCache[typeof(T)][id];
        }
        #endregion

        #region Accessors
        public static FishProperties GetFishProperties(string id) { return (FishProperties)GetProperties<FishProperties>(id); }
        #endregion
    }
}
