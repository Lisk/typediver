using Assets.Code.Managers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

public class Main : MonoBehaviour
{
    void Start()
    {
        Debug.Log("Hello Ocean");

        #region Pre-Features pass
        //System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(SceneOrganization).TypeHandle);
        //System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(typeof(RendererDispatcher).TypeHandle);
        #endregion

        #region Load static constructors
        List<Type> tasks = new List<Type>();
        List<string> namespaces = new List<string>()
            {
                "Assets.Code.Api",
                "Assets.Code.Managers",
            };

        foreach (string pickedNamespace in namespaces)
        {
            tasks.AddRange(from t in Assembly.GetExecutingAssembly().GetTypes()
                           where t.IsClass && t.Namespace == pickedNamespace
                           select t);
        }

        foreach (Type task in tasks)
        {
            if (!task.Name.StartsWith("<>c"))
                Debug.Log("Run static constructor for: " + task.Name);
            System.Runtime.CompilerServices.RuntimeHelpers.RunClassConstructor(task.TypeHandle);
        }

        Debug.Log("Main: Done loading static constructors!");
        #endregion

        // TESTING
        var angler = ContentManager.GetFishProperties("angler-fish");
        Debug.Log("id: " + angler.Id + ", shallowest: " + angler.Shallowest + ", deepest: " + angler.Deepest + ", funfact: " + angler.Funfact);
    }
}
