﻿using Assets.Code.DataStructures.Abstract;

namespace Assets.Code.DataStructures
{
    public class FishProperties : CsvProperties
    {
        //public string Id { get; set; }
        public int Shallowest { get; set; }
        public int Deepest { get; set; }
        public string Funfact { get; set; }
    }
}
